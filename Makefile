C3=bin/CAPITULO_3/
C4=bin/CAPITULO_4/

all: capitulo3 capitulo4

clean: clean_c3 clean_c4

capitulo3:listing_3.1 listing_3.2 listing_3.3 listing_3.4 listing_3.5 listing_3.6 listing_3.7 

listing_3.1: print-pid
listing_3.2: system
listing_3.3: fork
listing_3.4: fork-exec
listing_3.5: sigusr1
listing_3.6: zombie
listing_3.7: sigchld

print-pid:
	gcc -o $(C3)print-pid src/CAPITULO_3/print-pid.c
system:
	gcc -o $(C3)system src/CAPITULO_3/system.c
fork:
	gcc -o $(C3)fork src/CAPITULO_3/fork.c
fork-exec:
	gcc -o $(C3)fork-exec src/CAPITULO_3/fork-exec.c
sigusr1:
	gcc -o $(C3)sigusr1 src/CAPITULO_3/sigusr1.c
zombie:
	gcc -o $(C3)zombie src/CAPITULO_3/zombie.c
sigchld:
	gcc -o $(C3)sigchld src/CAPITULO_3/sigchld.c

clean_c3:	
	rm -f $(C3)* 


capitulo4: listing_4.1 listing_4.2 listing_4.3 listing_4.4 listing_4.5 listing_4.6 listing_4.7 listing_4.8 listing_4.9 listing_4.10 listing_4.11 listing_4.12 listing_4.13 listing_4.14 listing_4.15

listing_4.1: #thread-create
	gcc -o $(C4)thread-create src/CAPITULO_4/4.1/thread-create.c -lpthread
listing_4.2: #thread-create2
	gcc -o $(C4)thread-create2 src/CAPITULO_4/4.2/thread-create2.c -lpthread
listing_4.3: #thread-create2-revised
	gcc -o $(C4)thread-create2-revised src/CAPITULO_4/4.3/thread-create2-revised.c -lpthread
listing_4.4: #primes
	gcc -o $(C4)primes src/CAPITULO_4/4.4/primes.c -lpthread
listing_4.5: #detached
	gcc -o $(C4)detached src/CAPITULO_4/4.5/detached.c -lpthread
listing_4.6:# critical-section
	gcc -o $(C4)critical-section src/CAPITULO_4/4.6/critical-section.c -lpthread
listing_4.7:# tsd
	gcc -o $(C4)tsd src/CAPITULO_4/4.7/tsd.c -lpthread
listing_4.8: #cleanup
	gcc -o $(C4)cleanup src/CAPITULO_4/4.8/cleanup.c -lpthread
listing_4.9:# cxx-exit
	g++ -o $(C4)cxx-exit src/CAPITULO_4/4.9/cxx-exit.cpp -lpthread
listing_4.10: #job-queue1
	gcc -o $(C4)job-queue1 src/CAPITULO_4/4.10/job-queue1.c -lpthread
listing_4.11:# job-queue2
	gcc -o $(C4)job-queue2 src/CAPITULO_4/4.11/job-queue2.c -lpthread
listing_4.12:# job-queue3
	gcc -o $(C4)job-queue3 src/CAPITULO_4/4.12/job-queue3.c -lpthread
listing_4.13: #spin-condvar
	gcc -o $(C4)spin-condvar src/CAPITULO_4/4.13/spin-condvar.c -lpthread
listing_4.14: #condvar
	gcc -o $(C4)condvar src/CAPITULO_4/4.14/condvar.c -lpthread
listing_4.15: #thread-pid
	gcc -o $(C4)thread-pid src/CAPITULO_4/4.15/thread-pid.c -lpthread

clean_c4:
	rm -f $(C4)*
