#include <pthread.h>
#include <stdio.h>
#include <string.h>

/* An array of balances in accounts, indexed by account number.  */
float* account_balances;
/* Transfer DOLLARS from account FROM_ACCT to account TO_ACCT.  Return
0 if the transaction succeeded, or 1 if the balance FROM_ACCT is
too small.  */
int process_transaction (int from_acct, int to_acct, float dollars)
{
	int old_cancel_state;
	/* Check the balance in FROM_ACCT.  */
	if (account_balances[from_acct] < dollars)
		return 1;

	/* Begin critical section.  */
	pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, &old_cancel_state);
	/* Move the money.  */
	account_balances[to_acct] += dollars;
	account_balances[from_acct] -= dollars;
	/* End critical section.  */
	pthread_setcancelstate (old_cancel_state, NULL);

	return 0;
}

/* transfiere 1 dolar de la cuenta 1 a la cuenta 2 */
void* funcion_hilo()
{
	process_transaction(0,1,1);
	return NULL;
}

int main()
{
	float cuentas[] = {3,0}; // la cuenta 1 tiene 3 dolares y la cuenta 2 tiene 0
	account_balances = cuentas;
	printf("antes de la transaccion\n");
	printf("la cuenta 1 tiene %f dolares y la cuenta 2 tiene %f dolares\n",account_balances[0],account_balances[1]);

	pthread_t hilo1;
	pthread_create(&hilo1, NULL, &funcion_hilo, NULL);

	int resultado;
	pthread_join(hilo1, (void*)&resultado);

	printf("luego de la transaccion\n");
	printf("la cuenta 1 tiene %f dolares y la cuenta 2 tiene %f dolares",account_balances[0],account_balances[1]);
	return 0;
}