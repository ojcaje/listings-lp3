#include <malloc.h>
#include <pthread.h>
#include <stdio.h>

/* ========== ========== */
static pthread_key_t thread_log_key;	// la variable global en la que cada thread guardara su propia informacion
/* ========== ========== */

/* funcion limpiadora (recibe como argumento la key especifica automaticamente desde pthread_key_create) */
void close_thread_log(void* hilo_log_key)
{
	fclose((FILE*) hilo_log_key);
}

/* funcion que escribe un texto en el archivo del hilo */
void write_to_thread_log(char* texto)
{
	fputs(texto, (FILE*) pthread_getspecific(thread_log_key));
}

/* funcion de un hilo */
void* thread_function(void* args)
{
	/* generar el id de este hilo y cargarlo en el vector. Como son hilos distintos,
	   se creara un archivo distinto para cada hilo. (cada log file tendra el nombre de su thread) */
	char thread_log_filename[20];
	sprintf(thread_log_filename, "thread%d.log.txt", (int) pthread_self());
	
	/* abrir el archivo de log de este thread */
	FILE* thread_log;
	thread_log = fopen(thread_log_filename, "w");

	/* ========== almacenar el puntero al archivo en la variable especifica para este thread ========== */
	pthread_setspecific(thread_log_key, thread_log);
	/* ========== ========== */

	/* ejemplo de uso */
	write_to_thread_log(thread_log_filename);

	/* el trabajo del hilo va aqui... */

	return NULL;
}

int main()
{
	int i;
	pthread_t threads[5]; // variable a los hilos

	/* ========== ========== */
	pthread_key_create(&thread_log_key, close_thread_log); // crea una llave y especifica su funcion limpiadora
	/* ========== ========== */

	for (i=0; i<5; ++i)	// crear hilos
		pthread_create(&(threads[i]), NULL, thread_function, NULL);

	for(i=0; i<5; ++i)	// esperar a los hilos
		pthread_join(threads[i], NULL);

	return 0;

}