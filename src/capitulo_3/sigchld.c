#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>
#include <unistd.h>

sig_atomic_t child_exit_status;
void clean_up_child_process (int signal_number)
{
	/* Clean up the child process.  */
	printf("Limpiando el proceso hijo...\n");
	int status;
	wait (&status); // limpieza del hijo
	/* Store its exit status in a global variable.  */
	child_exit_status = status;
}
int main ()
{
	/* Handle SIGCHLD by calling clean_up_child_process.  */
	struct sigaction sigchld_action;
	memset (&sigchld_action, 0, sizeof (sigchld_action));
	sigchld_action.sa_handler = &clean_up_child_process;
	sigaction (SIGCHLD, &sigchld_action, NULL);
	/* Now do things, including forking a child process.  */
	pid_t child_pid;
	printf ("the main program process ID is %d\n", (int) getpid ());
	child_pid = fork ();
	if (child_pid != 0) {
		sleep (60); // dormir al padre para que se pueda probar la limpieza del hijo
	}
	else
	{
		return(0); // finalizar al hijo
	}

	return 0;
	
}